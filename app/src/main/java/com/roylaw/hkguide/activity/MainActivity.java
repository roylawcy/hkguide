package com.roylaw.hkguide.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.roylaw.hkguide.R;
import com.roylaw.hkguide.fragment.MainFragment;

/**
 * This is the main activity for the application.
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Initialize content fragment
        MainFragment mainFragment = MainFragment.newInstance();

        // Put content fragment into container layout
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .add(R.id.main_container_layout, mainFragment)
                .commit();
    }
}
