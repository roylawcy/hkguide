package com.roylaw.hkguide.utils;

import com.roylaw.hkguide.model.ApiResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

/**
 * This interface defines the API endpoint for the app
 */
public interface ApiEndpointInterface {
    // Request method and URL specified in the annotation
    @Headers("Content-Type: application/json; charset=utf8")

    @GET("api/cityguides?")
    Call<ApiResponse> getCityGuideList(
            @Query("fromItemIndex") int fromItemIndex,
            @Query("size") int size
    );

    @GET("api/shops?")
    Call<ApiResponse> getShopList(
            @Query("fromItemIndex") int fromItemIndex,
            @Query("size") int size
            );

    @GET("api/eats?")
    Call<ApiResponse> getEatList(
            @Query("fromItemIndex") int fromItemIndex,
            @Query("size") int size
    );
}
