package com.roylaw.hkguide.utils;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * This is a utils class used for setting up and getting the retrofit client and networking related stuff
 */
public class ApiClient {

    private static final String API_URL = "https://ekktfqwqgk.execute-api.ap-northeast-1.amazonaws.com/Prod/";
    private static Retrofit retrofit = null;

    /**
     * Config and get retrofit client object.
     * @return retrofit client
     */
    public static Retrofit getClient() {

        OkHttpClient client = new OkHttpClient();

        retrofit = new Retrofit.Builder()
                .baseUrl(API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        return retrofit;
    }


}
