package com.roylaw.hkguide.fragment;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.roylaw.hkguide.R;
import com.roylaw.hkguide.adapter.TabAdapter;


/**
 * This fragment mainly handle the tabs and swiping feature with tab layout and view pager
 */
public class MainFragment extends Fragment {

    /**
     * Factory method to create new instance of {@link MainFragment}.
     *
     * @return a new instance of {@link MainFragment}
     */
    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fragmentView = inflater.inflate(R.layout.fragment_main, container, false);

        // Initialize views
        TabLayout tabLayout = (TabLayout) fragmentView.findViewById(R.id.tab_layout);
        ViewPager viewPager = (ViewPager) fragmentView.findViewById(R.id.view_pager);

        // Config view pager and tab layout
        viewPager.setAdapter(new TabAdapter(viewPager.getContext(), getChildFragmentManager()));
        tabLayout.setupWithViewPager(viewPager);

        return fragmentView;
    }
}
