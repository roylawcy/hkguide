package com.roylaw.hkguide.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.roylaw.hkguide.R;
import com.roylaw.hkguide.adapter.CityInfoRecyclerViewAdapter;
import com.roylaw.hkguide.listener.EndlessRecyclerViewScrollListener;
import com.roylaw.hkguide.model.ApiResponse;
import com.roylaw.hkguide.model.CityInfo;
import com.roylaw.hkguide.utils.ApiClient;
import com.roylaw.hkguide.utils.ApiEndpointInterface;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * This fragment contains the list content of each tab.
 * Pass in info type in the factory method for different content.
 */
public class CityInfoListFragment extends Fragment {

    private static final String INFO_TYPE = "info type";
    // Define the size of data requested
    private static final int QUERY_SIZE = 10;
    // Define when to get new data
    private static final int VISIBLE_THRESHOLD_LIMIT = 5;

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private int mInfoType;
    private ArrayList<CityInfo> mCityInfos;
    private CityInfoRecyclerViewAdapter mAdapter;
    private EndlessRecyclerViewScrollListener mScrollListener;
    private ApiEndpointInterface mApiService;

    /**
     * Factory method to create new instance of {@Link CityInfoListFragment}.
     *
     * @param type data type for this list
     * @return new instance of {@Link CityInfoListFragment} for that data type
     */
    public static CityInfoListFragment newInstance(int type) {

        CityInfoListFragment fragment = new CityInfoListFragment();
        Bundle arguments = new Bundle();
        arguments.putInt(INFO_TYPE, type);
        fragment.setArguments(arguments);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Get data from arguments
        if (getArguments() != null) {
            mInfoType = getArguments().getInt(INFO_TYPE);
        }

        // Initialize objects
        mCityInfos = new ArrayList<>();

        // Initialize retrofit api service
        Retrofit retrofit = ApiClient.getClient();
        mApiService = retrofit.create(ApiEndpointInterface.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_general_list, container, false);

        // Initialize views
        RecyclerView recyclerView = (RecyclerView) fragmentView.findViewById(R.id.recycler_view);
        mSwipeRefreshLayout = (SwipeRefreshLayout) fragmentView.findViewById(R.id.swipe_refresh_layout);

        // Setup recycler view
        recyclerView.setHasFixedSize(true);

        // Use a linear layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(recyclerView.getContext());
        recyclerView.setLayoutManager(layoutManager);

        // Set the adapter
        mAdapter = new CityInfoRecyclerViewAdapter(mCityInfos);
        recyclerView.setAdapter(mAdapter);

        // Set an endless scroll listener for infinite scroll
        mScrollListener = new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                loadNextDataFromApi(totalItemsCount);
            }
        };
        mScrollListener.setVisibleThreshold(VISIBLE_THRESHOLD_LIMIT);
        recyclerView.addOnScrollListener(mScrollListener);

        // Set on refresh listener for recycler view
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // remove all items in list and reset scroll listener's state
                mCityInfos.clear();
                mAdapter.notifyDataSetChanged();
                mScrollListener.resetState();

                // load initial data again
                loadInitialData();
            }
        });

        return fragmentView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // load initial data if needed
        loadInitialData();
    }

    /**
     * Method to send API request to server to get next data
     *
     * @param fromItemIndex item index the response data should start with
     */
    public void loadNextDataFromApi(int fromItemIndex) {

        // initialize api request for each tab
        Call<ApiResponse> call = null;
        switch (mInfoType) {
            case CityInfo.INFO_TYPE_CITY_GUIDE:
                call = mApiService.getCityGuideList(fromItemIndex, QUERY_SIZE);
                break;

            case CityInfo.INFO_TYPE_SHOP:
                call = mApiService.getShopList(fromItemIndex, QUERY_SIZE);
                break;

            case CityInfo.INFO_TYPE_EAT:
                call = mApiService.getEatList(fromItemIndex, QUERY_SIZE);
                break;
        }

        // send API request and handle response
        if (call != null) {
            call.enqueue(new Callback<ApiResponse>() {
                @Override
                public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
                    int statusCode = response.code();

                    if (statusCode == 200) {
                        // Success case
                        ApiResponse apiResponse = response.body();
                        if (apiResponse != null) {
                            List<CityInfo> cityInfos = apiResponse.getCityInfos();
                            mCityInfos.addAll(cityInfos);
                            mAdapter.notifyDataSetChanged();
                        }
                    } else {
                        showSnackbar(R.string.message_fail_to_get_data);
                    }

                    endSwipeToRefresh();
                }

                @Override
                public void onFailure(Call<ApiResponse> call, Throwable t) {
                    t.printStackTrace();

                    showSnackbar(R.string.message_fail_to_get_data);

                    endSwipeToRefresh();
                }
            });
        }
    }

    /**
     * Show a snackbar with string of the passed in string resource id
     *
     * @param stringResId string resource id
     */
    private void showSnackbar(int stringResId) {
        // show error message on failure
        if (getView() != null) {
            Snackbar.make(getView(), stringResId, Snackbar.LENGTH_SHORT)
                    .show();
        }
    }

    /**
     * Stop refreshing animation for swipe refresh layout
     */
    private void endSwipeToRefresh() {
        // End refreshing animation
        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    /**
     * Load initial data if there is no data in the list
     */
    private void loadInitialData() {
        if (mCityInfos.size() == 0) {
            loadNextDataFromApi(0);
        }
    }
}
