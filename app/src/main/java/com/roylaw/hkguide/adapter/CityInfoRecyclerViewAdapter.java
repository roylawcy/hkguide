package com.roylaw.hkguide.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.roylaw.hkguide.R;
import com.roylaw.hkguide.model.CityInfo;

import java.util.ArrayList;

/**
 * Adapter for recycler view in {@link com.roylaw.hkguide.fragment.CityInfoListFragment}
 */
public class CityInfoRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final ArrayList<CityInfo> mCityInfos;

    public CityInfoRecyclerViewAdapter(ArrayList<CityInfo> cityInfos) {
        mCityInfos = cityInfos;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            default:
            case CityInfo.ITEM_TYPE_NORMAL: {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_normal, parent, false);

                return new NormalViewHolder(view);
            }

            case CityInfo.ITEM_TYPE_IMAGE_ONLY: {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_image_only, parent, false);

                return new ImageOnlyViewHolder(view);
            }
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            default:
            case CityInfo.ITEM_TYPE_NORMAL: {
                NormalViewHolder viewHolder = (NormalViewHolder) holder;

                // Map data into views
                viewHolder.mCityInfo = getItem(position);
                viewHolder.mTitleTextView.setText(getItem(position).title);
                viewHolder.mDescriptionTextView.setText(getItem(position).description);

                // Load image
                Glide.with(viewHolder.mImageView.getContext())
                        .load(getItem(position).imageUrl)
                        .crossFade()
                        .into(viewHolder.mImageView);

                break;
            }

            case CityInfo.ITEM_TYPE_IMAGE_ONLY: {
                ImageOnlyViewHolder viewHolder = (ImageOnlyViewHolder) holder;

                // Map data into views
                viewHolder.mCityInfo = getItem(position);

                // Load image
                Glide.with(viewHolder.mImageView.getContext())
                        .load(getItem(position).imageUrl)
                        .crossFade()
                        .into(viewHolder.mImageView);

                break;
            }
        }
    }

    /**
     * Get CityInfo item in the corresponding position
     *
     * @param position position
     * @return CityInfo item for the position
     */
    public CityInfo getItem(int position) {
        return mCityInfos.get(position);
    }

    @Override
    public int getItemViewType(int position) {
        return getItem(position).itemType;
    }

    @Override
    public int getItemCount() {
        return mCityInfos.size();
    }

    /**
     * View holder for item with image, title and description
     */
    public class NormalViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView mImageView;
        public final TextView mTitleTextView;
        public final TextView mDescriptionTextView;
        public CityInfo mCityInfo;

        public NormalViewHolder(View view) {
            super(view);
            mView = view;
            mImageView = (ImageView) view.findViewById(R.id.image_view);
            mTitleTextView = (TextView) view.findViewById(R.id.title_text_view);
            mDescriptionTextView = (TextView) view.findViewById(R.id.description_text_view);
        }
    }

    /**
     * View holder for item with image only
     */
    public class ImageOnlyViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView mImageView;
        public CityInfo mCityInfo;

        public ImageOnlyViewHolder(View view) {
            super(view);
            mView = view;
            mImageView = (ImageView) view.findViewById(R.id.image_view);
        }
    }
}
