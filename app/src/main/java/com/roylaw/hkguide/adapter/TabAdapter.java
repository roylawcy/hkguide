package com.roylaw.hkguide.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.roylaw.hkguide.R;
import com.roylaw.hkguide.fragment.CityInfoListFragment;
import com.roylaw.hkguide.model.CityInfo;

/**
 * Adapter for managing the tabs on the tab bar.
 */
public class TabAdapter extends FragmentPagerAdapter {

    private String mCityGuideString;
    private String mShopString;
    private String mEatString;

    public TabAdapter(Context context, FragmentManager fm) {
        super(fm);

        // initialize tab title strings
        mCityGuideString = context.getString(R.string.tab_city_guide);
        mShopString = context.getString(R.string.tab_shop);
        mEatString = context.getString(R.string.tab_eat);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return CityInfoListFragment.newInstance(CityInfo.INFO_TYPE_CITY_GUIDE);
            case 1:
                return CityInfoListFragment.newInstance(CityInfo.INFO_TYPE_SHOP);
            case 2:
                return CityInfoListFragment.newInstance(CityInfo.INFO_TYPE_EAT);
            default:
                // return empty fragment for not specific cases
                return new Fragment();
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return mCityGuideString;

            case 1:
                return mShopString;

            case 2:
                return mEatString;

            default:
                return "";
        }
    }

    @Override
    public int getCount() {
        return 3;
    }
}
