package com.roylaw.hkguide.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * POJO class for the API response
 */
public class ApiResponse {

    @SerializedName("cityInfos")
    @Expose
    private List<CityInfo> cityInfos = null;

    public List<CityInfo> getCityInfos() {
        return cityInfos;
    }

    public void setCityInfos(List<CityInfo> cityInfos) {
        this.cityInfos = cityInfos;
    }

}