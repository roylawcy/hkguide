package com.roylaw.hkguide.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Model class for city info object with different info types (city guide / shop / eat)
 */
public class CityInfo {

    public static final int ITEM_TYPE_NORMAL = 1;
    public static final int ITEM_TYPE_IMAGE_ONLY = 2;

    public static final int INFO_TYPE_CITY_GUIDE = 101;
    public static final int INFO_TYPE_SHOP = 102;
    public static final int INFO_TYPE_EAT = 103;

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("title")
    @Expose
    public String title;

    @SerializedName("description")
    @Expose
    public String description;

    @SerializedName("imageUrl")
    @Expose
    public String imageUrl;

    @SerializedName("itemType")
    @Expose
    public int itemType;

    @SerializedName("infoType")
    @Expose
    public int infoType;
}
