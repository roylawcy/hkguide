# HKGuide - Read Me #

HKGuide is an Android application that demonstrating:

* The use of tabs and view pager
* Retrieve JSON data from API
* Display data with different types of item in a list
* Swipe from top to refresh a list
* Scroll vertically to load more content (infinite scrolling)

## How to Start ##
1. Please switch to branch with the latest version (see tag '1.0.1 (3)').
2. Clone the repository to local, and open it with Android Studio.
3. Run the application.

You may also download the apk file [here](https://s3-ap-northeast-1.amazonaws.com/hkguide/hkguide-debug.apk).


## Third Party Libraries Used ##
* Retrofit2
* OkHttp3
* Gson
* Glide

## API ##
The sample API is hosted on AWS (API Gateway, Lambda and S3).